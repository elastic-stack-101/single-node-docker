# Curator

Curator is a tool from Elastic to help manage your Elasticsearch cluster. It performs many operations on your Elasticsearch indices, from delete to snapshot to shard allocation routing.

Reference:
  * https://github.com/elastic/curator
  * https://www.elastic.co/guide/en/elasticsearch/client/curator/current/index.html

### Configuration File

config_file.yml

Reference:
 * https://www.elastic.co/guide/en/elasticsearch/client/curator/current/configfile.html

### Action File

action_file.yml

Reference:
 * [Documentation](https://www.elastic.co/guide/en/elasticsearch/client/curator/current/actionfile.html)
 * [More examples of actions](https://github.com/elastic/curator/tree/master/examples/actions)

 
