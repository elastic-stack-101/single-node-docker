
# Elastic Stack

* Current version 6.5.0 oss.
* TODOs
  - [ ] Secure Kibana using [Search Guide](https://github.com/floragunncom/search-guard)
  - [ ] Cluster Configuration
  - [ ] Add [Alerting](https://www.elastic.co/products/stack/alerting)
  - [ ] Update README and README for sub-folders.

### Overview

Formerly known as ELK stack, it has been re-branded as Elastic Stack. It comprises of 3 main products:
* **E**lastic search - distributed JSON-based search and analytics engine 
* **L**ogstash - server-side data collection and log parsing engine
* **K**ibana - an analytics and visualisation platform called Kibana

### Other Tools

* Kdashboard - Import dashboards for kibana
* Curator - Manage your elasticsearch cluster by performing operations on indices etc.

## READMEs

Refer to the individual READMEs
* [Elasticsearch](elasticsearch)
* [Logstash](logstash)
* [Kibana](kibana)
* [Dasboards for Kibana](kdashboards)
* [Curator](curator)
